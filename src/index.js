import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './components/App';
import Images from './components/Images'


render(
  <div>
    <App />
    <Images />
  </div>,
  document.getElementById('root')
);
